#!/usr/bin/env node

const program = require("commander");
const animate = require("./animate").default;

program.option(
  "-t, --text <value> <required>",
  "The comma delimited text for the generated video e.g. 'I like dogs,I like cats'"
);
program.option(
  "-a, --audio <value> <optional>",
  "The audio for the generated video e.g. ./audio.mp3"
);
program.option(
  "-o, --output <value> <required>",
  "The output .mp4 path for the video e.g. ./dance.mp4"
);
program.parse(process.argv);

const run = async () => {
  const text = program.text.split(",");
  const audio = program.audio;
  const output = program.output;

  if (!text.length) {
    throw new Error(
      "Comma delimited text is required to generated the video e.g. 'I like dogs,I like cats'"
    );
  }

  if (audio && audio.endsWith(".mp3")) {
    throw new Error("Audio input must be an .mp3 file");
  }

  if (!output) {
    throw new Error("Output path is required");
  }

  if (!output.endsWith(".mp4")) {
    throw new Error("Output path must end in .mp4");
  }

  await animate(text, audio, output);
};

run();
