const puppeteer = require("puppeteer");
const { promisify } = require("util");
const program = require("commander");
const exec = promisify(require("child_process").exec);
const fs = require("fs");
const path = require("path");
const progress = require("cli-progress");

let browser;
const openBrowser = async () => {
  const args = [
    "--window-size=2000,2000",
    "--no-sandbox",
    "--disable-setuid-sandbox",
    "--disable-infobars",
    "--window-position=0,0",
    "--ignore-certifcate-errors",
    "--ignore-certifcate-errors-spki-list",
    '--user-agent="Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36"',
    '--lang="Accept-Language: en-GB,en-US;q=0.9,en;q=0.8"'
  ];

  const options = {
    args,
    headless: true,
    ignoreHTTPSErrors: true,
    defaultViewport: {
      width: 2000,
      height: 2000
    }
  };

  browser = await puppeteer.launch(options);
};

const htmlFilePath = path.resolve("./lib/index.html");

// generate a video
const generate = async (text, audio, output) => {
  const page = await browser.newPage();
  let frame = 0;
  const bar = new progress.SingleBar(
    {
      format:
        "{bar} {percentage}% | {value}/{total} frames | {scene}/{totalScenes} scenes"
    },
    progress.Presets.shades_classic
  );
  try {
    await page.goto(`file:///${htmlFilePath}`, {
      waitUntil: "networkidle2"
    });

    await page.evaluate(
      async ({ text }) => {
        console.log(text);
        window.start(text);
      },
      { text }
    );
    let running = true;

    // we will seek through the timeline of our animation in milliseconds
    // our target frame rate is 24 frames per second
    // 1 second = 1000 milliseconds
    // 1000 / 24 = 41.6
    // so we move the timeline forward by 41.6 milliseconds per frame
    const milliPerFrame = 41.6;

    while (running) {
      const { finished, scene, duration, current } = await page.evaluate(
        async milliPerFrame => {
          // animation.seek jumps to a specific time in the animation (in milliseconds).
          // https://animejs.com/documentation/#seekAnim
          window.animation.seek(window.next);
          window.next += milliPerFrame;
          return {
            finished: window.finished,
            scene: window.scene,
            duration: window.animation.duration,
            current: window.animation.currentTime
          };
        },
        milliPerFrame
      );
      bar.start(
        Math.ceil(duration / milliPerFrame),
        Math.ceil(current / milliPerFrame),
        {
          scene,
          totalScenes: text.length
        }
      );

      // take a screenshot of the current frame
      await page.screenshot({
        type: "jpeg",
        path: `./lib/frames/${frame}.jpeg`,
        quality: 100,
        clip: { x: 0, y: 0, width: 1280, height: 720 }
      });

      frame += 1;
      if (finished) {
        running = false;
      }
    }

    if (audio) {
      await exec(
        `ffmpeg -i ./lib/frames/%d.jpeg -i ${audio} -s 1280x720 -c:v libx264 -preset slow -profile:v high -crf 18 -pix_fmt yuv420p -movflags +faststart -c:a aac -coder 1 -b:a 384k -g 30 -bf 2 -profile:a aac_low -shortest ${output}`
      );
    } else {
      await exec(
        `ffmpeg -i ./lib/frames/%d.jpeg -s 1280x720 -c:v libx264 -preset slow -profile:v high -crf 18 -pix_fmt yuv420p -movflags +faststart -c:a aac -coder 1 -b:a 384k -g 30 -bf 2 -profile:a aac_low -shortest ${output}`
      );
    }
  } catch (e) {
    console.log(e.message);
  }

  await page.close();

  // cleanup
  for (let i = 0; i < frame; i++) {
    try {
      fs.unlinkSync(`./lib/frames/${i}.jpeg`);
    } catch (e) {}
  }

  bar.stop();
};

exports.default = async (text, audio, output) => {
  await openBrowser();
  await generate(text, audio, output);
  await browser.close();
};
