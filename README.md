# Text To Animation

The text-to-animation library enables the generation of an animation given an input text.

## Requirements

`text-to-animation` requires Node v10.16.3+ and [ffmpeg](https://www.ffmpeg.org/). If you do not have ffmpeg installed already the easiest way to install it is [via homebrew](https://github.com/fluent-ffmpeg/node-fluent-ffmpeg/wiki/Installing-ffmpeg-on-Mac-OS-X).

## Installation

1. Clone this repo.
2. `npm install` or `yarn install`

## Use

`text-to-animation` comes with a cli that accepts the following arguments:

-t, --text <value> <required> The comma delimited text for the generated video e.g. 'I like dogs,I like cats'

-a, --audio <value> <optional> The optional audio for the generated video e.g. audio.mp3

-o, --output <value> <required> The output .mp4 path for the video e.g. dance.mp4

This repo comes with an example audio file that you can use to create an animation of your own.

Example:

`node lib/cli.js -t "I like dogs,I like cats" -a audio.mp3 -o pets.mp4`

## How it works

`text-to-animation` works by using a headless chrome browser via [puppeteer](https://github.com/GoogleChrome/puppeteer) to load a html file that animates the input text with [animejs](https://animejs.com/). The animation timeline is controlled and as it progresses a screenshot is taken for each new frame. Finally, [ffmpeg](https://www.ffmpeg.org/) is used to stitch screenshots together into a single fluid animation.
